﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour {

    private Rigidbody rb;
    public float force;
    public int damage;

    void OnCollisionEnter(Collision col)
    {
        if(col.gameObject.tag == "Enemy")
        {
            Destroy(this.gameObject);
        }
        else
        {
            
        }
        
    }

    private void Awake()
    {
        rb = gameObject.GetComponent<Rigidbody>();
    }

    public void Fire()
    {
        rb.AddForce(transform.forward * force);
    }
}
