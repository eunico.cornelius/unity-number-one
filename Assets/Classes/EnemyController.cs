﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour {

    public int health;

    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.tag == "damageBullet") {
            Projectile projectile = col.gameObject.GetComponent<Projectile>();
            int damage = projectile.damage;
            GetHit(damage);
            Debug.Log("Hit!");
        }
    }
	
    public void GetHit(int damage)
    {
        health -= damage;
        Debug.Log("Current health: " + health);

        if (health < 1)
        {
            Destroy(this.gameObject);
        }
    }

}
